package com.obi.awesomenavcomponent.ui.user.detail

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomenavcomponent.data.response.users.AlbumPhotoResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumResponse
import com.obi.awesomenavcomponent.databinding.ItemAlbumBinding
import com.obi.awesomenavcomponent.databinding.ItemPhotoBinding
import com.obi.awesomenavcomponent.utils.AutoUpdateableAdapter
import com.obi.awesomenavcomponent.utils.bankImage
import kotlin.properties.Delegates

class AlbumPhotoAdapter (listPost: MutableList<AlbumPhotoResponse>) :
    RecyclerView.Adapter<AlbumPhotoAdapter.ViewHolder>(), AutoUpdateableAdapter {
    private var items: List<AlbumPhotoResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListPhoto(datas: MutableList<AlbumPhotoResponse>) {
        items = datas
    }

    override fun getItemCount(): Int = if (items.size > 9) 9 else items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    class ViewHolder(val binding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photo: AlbumPhotoResponse) {
            binding.apply {
                ivPhoto.bankImage(photo.thumbnailUrl)
            }
        }
    }

}