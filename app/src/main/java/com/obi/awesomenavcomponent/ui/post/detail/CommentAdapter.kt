package com.obi.awesomenavcomponent.ui.post.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomenavcomponent.R
import com.obi.awesomenavcomponent.data.response.posts.CommentResponse
import com.obi.awesomenavcomponent.data.response.posts.DataPostResponse
import com.obi.awesomenavcomponent.databinding.ItemCommentBinding
import com.obi.awesomenavcomponent.databinding.ItemPostBinding
import com.obi.awesomenavcomponent.ui.post.list.PostAdapter
import com.obi.awesomenavcomponent.utils.AutoUpdateableAdapter
import com.obi.awesomenavcomponent.utils.navigateSafely
import kotlin.properties.Delegates

class CommentAdapter (
    listPost: MutableList<CommentResponse>,
    private var itemCount: Int) :
    RecyclerView.Adapter<CommentAdapter.ViewHolder>(), AutoUpdateableAdapter {
    private var items: List<CommentResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListComment(datas: MutableList<CommentResponse>) {
        items = datas
    }

    override fun getItemCount() = if (items.size > itemCount) itemCount else items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    class ViewHolder(val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: CommentResponse) {
            binding.apply {
                tvUserName.text = comment.email
                tvBody.text = comment.body
            }
        }
    }

}