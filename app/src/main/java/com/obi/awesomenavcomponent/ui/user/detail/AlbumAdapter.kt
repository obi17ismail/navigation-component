package com.obi.awesomenavcomponent.ui.user.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomenavcomponent.R
import com.obi.awesomenavcomponent.data.response.posts.DataPostResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumPhotoResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumResponse
import com.obi.awesomenavcomponent.data.response.users.UserResponse
import com.obi.awesomenavcomponent.data.uiresponse.BaseUiResponse
import com.obi.awesomenavcomponent.data.uiresponse.Status
import com.obi.awesomenavcomponent.databinding.ItemAlbumBinding
import com.obi.awesomenavcomponent.databinding.ItemPostBinding
import com.obi.awesomenavcomponent.ui.post.detail.PostDetailFragment
import com.obi.awesomenavcomponent.ui.post.list.PostAdapter
import com.obi.awesomenavcomponent.utils.AutoUpdateableAdapter
import com.obi.awesomenavcomponent.utils.navigateSafely
import com.obi.awesomenavcomponent.utils.showToast
import com.obi.awesomenavcomponent.utils.toVisible
import com.obi.awesomenavcomponent.viewmodel.user.UserViewModel
import timber.log.Timber
import kotlin.properties.Delegates

class AlbumAdapter (
    listPost: MutableList<AlbumResponse>,
    private var listPhoto: MutableList<AlbumPhotoResponse>) :
    RecyclerView.Adapter<AlbumAdapter.ViewHolder>(), AutoUpdateableAdapter {

    private var items: List<AlbumResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemAlbumBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListAlbum(datas: MutableList<AlbumResponse>, photos: MutableList<AlbumPhotoResponse>) {
        items = datas
        listPhoto = photos
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], listPhoto)

    class ViewHolder(val binding: ItemAlbumBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(album: AlbumResponse, photos: List<AlbumPhotoResponse>) {
            var dataPhotos: List<AlbumPhotoResponse> = emptyList()
            binding.apply {
                tvAlbum.text = album.title
                rvPhoto.apply {
                    layoutManager = GridLayoutManager(context, 3)
                    adapter = AlbumPhotoAdapter(mutableListOf())
                }
                photos.forEach {
                    if(it.albumId == album.id){
                        dataPhotos = dataPhotos + listOf(it)
                    }
                }
                Log.d("CekSound", dataPhotos.size.toString())
                rvPhoto.adapter?.let { adapter ->
                    if (adapter is AlbumPhotoAdapter) {
                        val datas = dataPhotos.toMutableList()
                        adapter.setListPhoto(datas)
                    }
                }
            }
        }
    }

}