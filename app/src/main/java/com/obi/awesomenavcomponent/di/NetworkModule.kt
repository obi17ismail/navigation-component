package com.obi.awesomenavcomponent.di

import android.content.Intent
import com.obi.awesomenavcomponent.BuildConfig
import com.obi.awesomenavcomponent.network.AppService
import com.obi.awesomenavcomponent.network.RemoteDataSource
import com.obi.awesomenavcomponent.utils.AppConstant.API_TIMEOUT
import com.skydoves.sandwich.coroutines.CoroutinesResponseCallAdapterFactory
import com.skydoves.sandwich.interceptors.EmptyBodyInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Headers.Companion.toHeaders
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        var httpClient = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(interceptor)
        }

        httpClient.addNetworkInterceptor(
            Interceptor { chain ->
                val original = chain.request()
                val mapHeaders = HashMap<String, String>()

                var headers = original.headers

                if (headers.size > 0) {
                    for (i in 0 until headers.size) {
                        if (headers.name(i) == "Content-Type") {
                            mapHeaders[headers.name(i)] = "application/json"
                        } else {
                            mapHeaders[headers.name(i)] = headers.value(i)
                        }
                    }
                }

                headers = mapHeaders.toHeaders()

                val request = original.newBuilder()
                    .headers(headers)
                    .build()

                val response = chain.proceed(request)
                response.newBuilder().build()
            }
        )

        httpClient.connectTimeout(API_TIMEOUT, TimeUnit.MINUTES)
        httpClient.readTimeout(API_TIMEOUT, TimeUnit.MINUTES)
        httpClient.addInterceptor(EmptyBodyInterceptor)

        return httpClient.build()
    }

    @Provides
    @Singleton
    @Named("mainRetrofit")
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutinesResponseCallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideAppService(@Named("mainRetrofit") retrofit: Retrofit): AppService {
        return retrofit.create(AppService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(
        appService: AppService
    ): RemoteDataSource {
        return RemoteDataSource(appService)
    }
}