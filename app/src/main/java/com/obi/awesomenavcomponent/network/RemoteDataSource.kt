package com.obi.awesomenavcomponent.network

import com.obi.awesomenavcomponent.data.response.posts.CommentResponse
import com.obi.awesomenavcomponent.data.response.posts.DataPostResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumPhotoResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumResponse
import com.obi.awesomenavcomponent.data.response.users.UserResponse
import com.skydoves.sandwich.ApiResponse
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val appService: AppService
) {
    // POST
    suspend fun getPosts(): ApiResponse<List<DataPostResponse>> {
        return appService.getPosts()
    }
    suspend fun getPostId(id: String): ApiResponse<DataPostResponse> {
        return appService.getPostId(id)
    }

    // COMMENT
    suspend fun getCommentPostId(postId: String): ApiResponse<List<CommentResponse>> {
        return appService.getCommentPostId(postId)
    }

    // USER
    suspend fun getUser(): ApiResponse<List<UserResponse>> {
        return appService.getUser()
    }
    suspend fun getUserId(id: String): ApiResponse<UserResponse> {
        return appService.getUserId(id)
    }

    // ALBUM
    suspend fun getAlbumUserId(userId: String): ApiResponse<List<AlbumResponse>> {
        return appService.getAlbumUserId(userId)
    }
    suspend fun getAlbumIdPhoto(id: String): ApiResponse<List<AlbumPhotoResponse>> {
        return appService.getAlbumIdPhoto(id)
    }

    // PHOTO
    suspend fun getPhotos(): ApiResponse<List<AlbumPhotoResponse>> {
        return appService.getPhotos()
    }
}