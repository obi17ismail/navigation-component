package com.obi.awesomenavcomponent.network

object API {
    // POST
    const val POSTS = "posts"
    const val POSTS_ID = "$POSTS/{id}"
    const val POSTS_ID_COMMENTS = "$POSTS/{postId}/comments"

    // USER
    const val USERS = "users"
    const val USERS_ID = "$USERS/{id}"
    const val USERS_ID_ALBUMS = "$USERS/{id}/albums"
    const val USERS_ID_TODOS = "$USERS/{id}/todos"
    const val USERS_ID_POSTS = "$USERS/{id}/posts"

    // ALBUM
    const val ALBUMS_USER_ID = "users/{userId}/albums"
    const val ALBUMS_ID_PHOTOS = "albums/{id}/photos"

    // PHOTO
    const val PHOTOS = "photos"
}