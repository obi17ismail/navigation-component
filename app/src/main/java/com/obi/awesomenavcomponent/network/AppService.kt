package com.obi.awesomenavcomponent.network

import com.obi.awesomenavcomponent.data.response.posts.CommentResponse
import com.obi.awesomenavcomponent.data.response.posts.DataPostResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumPhotoResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumResponse
import com.obi.awesomenavcomponent.data.response.users.UserResponse
import com.obi.awesomenavcomponent.network.API.ALBUMS_ID_PHOTOS
import com.obi.awesomenavcomponent.network.API.ALBUMS_USER_ID
import com.obi.awesomenavcomponent.network.API.PHOTOS
import com.obi.awesomenavcomponent.network.API.POSTS
import com.obi.awesomenavcomponent.network.API.POSTS_ID
import com.obi.awesomenavcomponent.network.API.POSTS_ID_COMMENTS
import com.obi.awesomenavcomponent.network.API.USERS
import com.obi.awesomenavcomponent.network.API.USERS_ID
import com.skydoves.sandwich.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface AppService {
    // POST
    @GET(POSTS)
    suspend fun getPosts(): ApiResponse<List<DataPostResponse>>

    @GET(POSTS_ID)
    suspend fun getPostId(
        @Path("id") id: String
    ): ApiResponse<DataPostResponse>

    // COMMENT
    @GET(POSTS_ID_COMMENTS)
    suspend fun getCommentPostId(
        @Path("postId") postId: String
    ): ApiResponse<List<CommentResponse>>

    // USER
    @GET(USERS)
    suspend fun getUser(): ApiResponse<List<UserResponse>>

    @GET(USERS_ID)
    suspend fun getUserId(
        @Path("id") id: String
    ): ApiResponse<UserResponse>

    // ALBUM
    @GET(ALBUMS_USER_ID)
    suspend fun getAlbumUserId(
        @Path("userId") userId: String
    ): ApiResponse<List<AlbumResponse>>

    @GET(ALBUMS_ID_PHOTOS)
    suspend fun getAlbumIdPhoto(
        @Path("id") id: String
    ): ApiResponse<List<AlbumPhotoResponse>>

    // PHOTO
    @GET(PHOTOS)
    suspend fun getPhotos(): ApiResponse<List<AlbumPhotoResponse>>
}