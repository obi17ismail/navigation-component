package com.obi.awesomenavcomponent.utils

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.google.android.material.snackbar.Snackbar

fun View.toVisible() {
    this.visibility = View.VISIBLE
}

fun View.toInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.toGone() {
    this.visibility = View.GONE
}

fun ImageView.bankImage(url: String) {
    val imageLoader = ImageLoader.Builder(this.context)
        .componentRegistry { add(SvgDecoder(this@bankImage.context)) }
        .build()

    val request = ImageRequest.Builder(this.context)
        .crossfade(true)
        .crossfade(200)
        .data(url)
        .target(this)
        .build()

    imageLoader.enqueue(request)
}

fun Context.showToast(message: String?, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.showSnackbar(view: View, message: String?, color: Int) {
    val snack = Snackbar.make(view, message!!, Snackbar.LENGTH_LONG)
    snack.view.setBackgroundColor(ContextCompat.getColor(this, color))
    return snack.show()
}