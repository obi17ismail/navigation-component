package com.obi.awesomenavcomponent.utils

object AppConstant {
    // network util
    const val API_TIMEOUT: Long = 1
    const val CODE = "code"
    const val ERROR_MESSAGE = "errorMessage"

    // date format
    const val TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val API_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val SLASH_DATE_FORMAT = "dd/MM/yyyy"
    const val DEFAULT_DATE_FORMAT = "EEEE, dd MMMM yyyy"
    const val DATE_FORMAT = "dd MMMM yyyy"
    const val TIME_FORMAT_DATE = "HH:mm"
    const val DATE_PARSE_FORMAT = "dd MMMM yyyy HH:mm"
    const val DATE_PARSE_DETAIL_FORMAT = "EEEE, dd MMMM yyyy HH:mm"

    // urrency format
    const val CURRENCY_FORMAT = "#,###.##"

    // important string message
    const val DATA_IS_EMPTY = "Data kosong"
    const val SOMETHING_WRONG = "Terjadi kesalahan"
    const val EMPTY_LIST = "Tidak Ada Data"
    const val UNAUTHORIZED = "Unauthorized"

    const val SPLASH_DURATION : Long = 2000 //in millisecond
}