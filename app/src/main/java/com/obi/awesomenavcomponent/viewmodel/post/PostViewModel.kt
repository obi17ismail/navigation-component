package com.obi.awesomenavcomponent.viewmodel.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.obi.awesomenavcomponent.data.response.posts.CommentResponse
import com.obi.awesomenavcomponent.data.response.posts.DataPostResponse
import com.obi.awesomenavcomponent.data.uiresponse.BaseUiResponse
import com.obi.awesomenavcomponent.repositories.PostRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(private val postRepository: PostRepository) :
    ViewModel() {
    fun getPosts(): LiveData<BaseUiResponse<List<DataPostResponse>>> {
        return postRepository.getPosts().asLiveData(viewModelScope.coroutineContext)
    }
    fun getPostId(id: String): LiveData<BaseUiResponse<DataPostResponse>> {
        return postRepository.getPostId(id).asLiveData(viewModelScope.coroutineContext)
    }
    fun getCommentPostId(postId: String): LiveData<BaseUiResponse<List<CommentResponse>>> {
        return postRepository.getCommentPostId(postId).asLiveData(viewModelScope.coroutineContext)
    }
}