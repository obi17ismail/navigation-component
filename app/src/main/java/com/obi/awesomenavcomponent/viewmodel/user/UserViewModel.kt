package com.obi.awesomenavcomponent.viewmodel.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.obi.awesomenavcomponent.data.response.users.AlbumPhotoResponse
import com.obi.awesomenavcomponent.data.response.users.AlbumResponse
import com.obi.awesomenavcomponent.data.response.users.UserResponse
import com.obi.awesomenavcomponent.data.uiresponse.BaseUiResponse
import com.obi.awesomenavcomponent.repositories.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val userRepository: UserRepository) :
    ViewModel() {
    fun getUser(): LiveData<BaseUiResponse<List<UserResponse>>> {
        return userRepository.getUser().asLiveData(viewModelScope.coroutineContext)
    }
    fun getUserId(id: String): LiveData<BaseUiResponse<UserResponse>> {
        return userRepository.getUserId(id).asLiveData(viewModelScope.coroutineContext)
    }

    // USER ALBUM
    fun getAlbumUserId(userId: String): LiveData<BaseUiResponse<List<AlbumResponse>>> {
        return userRepository.getAlbumUserId(userId).asLiveData(viewModelScope.coroutineContext)
    }
    fun getAlbumIdPhoto(id: String): LiveData<BaseUiResponse<List<AlbumPhotoResponse>>> {
        return userRepository.getAlbumIdPhoto(id).asLiveData(viewModelScope.coroutineContext)
    }

    // USER PHOTO
    fun getPhotos(): LiveData<BaseUiResponse<List<AlbumPhotoResponse>>> {
        return userRepository.getPhotos().asLiveData(viewModelScope.coroutineContext)
    }
}