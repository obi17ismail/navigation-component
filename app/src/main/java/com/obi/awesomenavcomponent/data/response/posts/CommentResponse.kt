package com.obi.awesomenavcomponent.data.response.posts

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class CommentResponse (
    @SerializedName("postId")
    var postId: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("body")
    var body: String
)