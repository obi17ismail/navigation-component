package com.obi.awesomenavcomponent.data.uiresponse

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}