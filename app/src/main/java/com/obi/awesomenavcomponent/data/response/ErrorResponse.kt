package com.obi.awesomenavcomponent.data.response

import com.google.gson.annotations.SerializedName

data class ErrorResponse<T>(
    @SerializedName("statusCode")
    val statusCode : Int,
    @SerializedName("message")
    val message : String?,
    @SerializedName("data")
    val data : T?
)
