package com.obi.awesomenavcomponent.data.response.users

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class AlbumPhotoResponse (
    @SerializedName("albumId")
    var albumId: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("url")
    var url: String,
    @SerializedName("thumbnailUrl")
    var thumbnailUrl: String
)